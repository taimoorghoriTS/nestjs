import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
// import configuration from './config/index';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    UserModule,
    ConfigModule.forRoot({ 
      // load: [configuration],
      isGlobal : true
    }),
    MongooseModule.forRoot("mongodb+srv://taimooor:taimoor@cluster0.w7wgn.mongodb.net/nestJs?retryWrites=true&w=majority")
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }

