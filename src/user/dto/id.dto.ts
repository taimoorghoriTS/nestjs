import { IsDefined, IsString } from 'class-validator'
export class IdDto {

    @IsString()
    @IsDefined()
    id: string;

}

