import { IsDefined, IsEmail, IsString } from 'class-validator'
export class UserDto {
    @IsString()
    @IsDefined()
    name: string;

    @IsString()
    @IsDefined()
    @IsEmail()
    email: string;

    @IsString()
    @IsDefined()
    password: string;

}