import { Controller, Post, Body, Get, Param, Patch, Delete, UsePipes, ValidationPipe, UseFilters } from '@nestjs/common';
import { UserService } from './user.service';
import { UserDto } from './dto/user.dto';
import { IdDto } from './dto/id.dto';
import { User } from './interface/user.interface'
import { DeleteOne } from './interface/delete.interface';
import { HttpExceptionFilter } from './filter';

@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) { }

    @Post()
    @UsePipes(new ValidationPipe())
    async addUser(
        @Body() body: UserDto
    ) {
        const generatedId = await this.userService.addUser(body)
        return { id: generatedId }
    }

    @Get()
    getAllUsers(): Promise<User[]> {
        return this.userService.getAllUsers()
    }

    @Get(":id")
    @UsePipes(new ValidationPipe())
    getUser(@Param() param: IdDto): Promise<User> {
        return this.userService.getUser(param.id)
    }

    @Patch(":id")
    @UsePipes(new ValidationPipe())
    updateUser(
        @Param() param: IdDto,
        @Body() body: User,
    ): Promise<User> {
        return this.userService.updateUser(param, body)
    }

    @Delete(":id")
    @UseFilters(new HttpExceptionFilter())
    async deleteUser(
        @Param() param: IdDto,
    ): Promise<DeleteOne> {
        return await this.userService.deleteUser(param)
    }


}

