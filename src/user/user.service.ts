import { Injectable, NotFoundException, UseFilters } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { User } from './interface/user.interface'
import { UserDto } from './dto/user.dto';
import { IdDto } from './dto/id.dto';
import { DeleteOne } from './interface/delete.interface';
@Injectable()
export class UserService {

    constructor(@InjectModel("user") private readonly userModel: Model<User>) { }

    async addUser(body : UserDto) {
        const newUser = new this.userModel({ name :  body.name,email :  body.email, password: body.password })
        const result = await newUser.save()
        return result.id as string
    }

    async getAllUsers() {
        const allUsers = this.userModel.find()
        return allUsers
    }

    getUser(id: string) {
        const user = this.findUser(id)
        return user
    }

    async updateUser(param: IdDto, body: User) {
        const user = await this.findUser(param.id)
        if (body.name) user.name = body.name
        if (body.email) user.email = body.email
        if (body.password) user.password = body.password
        return await user.save()
    }
    
    async deleteUser(param: IdDto) : Promise<DeleteOne> {
        const deletedUser : DeleteOne = await this.userModel.deleteOne({ _id: param.id })
        if (!deletedUser.deletedCount) {
            throw new NotFoundException('Could not found user.')
        }
        return deletedUser
    }

    private async findUser(id: string): Promise<User> {
        let user
        try {
            user = await this.userModel.findById(id)
            if (!user) throw new NotFoundException('Could not find user.')
        } catch (error) {
            throw new NotFoundException('Could not find user.')
        }
        return user
    }
 
}