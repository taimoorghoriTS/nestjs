import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException, HttpException, HttpStatus } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';

@Injectable()
export class JoiValidationPipe implements PipeTransform<any> {
    async transform(value: any, metadata: ArgumentMetadata) {


        console.log(metadata , 'p')
        if (value instanceof Object && this.isEmpty(value)) {
            throw new HttpException('Validation failed : ', HttpStatus.BAD_REQUEST);
        }
        const { metatype } = metadata

        if (!metatype || this.toValidate(metatype)) {
            return value
        }

        const object = plainToClass(metatype, value);
        const errors = await validate(object);
        if (errors.length > 0) {
            // throw new BadRequestException('Validation failed by joi');
            throw new HttpException(`Validation failed : ${this.formatErrors(errors)}` , HttpStatus.BAD_REQUEST);
        }
        return value;
    }

    private toValidate(metatype: Function): boolean {
        const types: Function[] = [String, Boolean, Number, Array, Object];
        return !types.includes(metatype);
    }

    private isEmpty(value: any) {
        if (Object.keys(value).length > 0) {
            return false
        }
        return true
    }

    private formatErrors(errors: any[]) {
        return errors.map(err => {
            for (let property in err.constraints) {
                return err.constraints[property]
            }
        })
            .join(', ')
    }


}


// custom request validation by joi